import React from 'react';
import CSS from 'csstype';

interface MenuProps {
}

export default function Menu(props: MenuProps) {
    const containerStyle: CSS.Properties = {
        padding: "1em",
    }
    return (
        <div style={containerStyle}> 
            This is a menu
        </div>
    );
}