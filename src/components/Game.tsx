import React, { useRef, useState } from 'react';
import { Stage, Layer, Rect } from 'react-konva';
import './Game.css'

type SpaceShape = 'square' | 'hexagon'
export type BoardSpace = "empty" | "red" | "green" | "blue"

interface DrawingAreaProps {
    height: number;
    width: number;
    boardState: {
        spaceShape: SpaceShape,
        boardData: BoardSpace[][]
    }
}
const DrawingArea = (props: DrawingAreaProps) => {
    const [lines, setLines] = useState<any>([]);
    const isDrawing = useRef(false);


    const handleMouseDown = (e: any) => {
        isDrawing.current = true;
        const pos = e.target.getStage().getPointerPosition();
        setLines([...lines, { points: [pos.x, pos.y] }]);
    };

    const handleMouseMove = (e: any) => {
        // no drawing - skipping
        if (!isDrawing.current) {
            return;
        }
        const stage = e.target.getStage();
        const point = stage.getPointerPosition();

        // To draw line
        let lastLine: any = lines[lines.length - 1];

        if (lastLine) {
            // add point
            lastLine.points = lastLine.points.concat([point.x, point.y]);

            // replace last
            lines.splice(lines.length - 1, 1, lastLine);
            setLines(lines.concat());
        }

    };

    const handleMouseUp = () => {
        isDrawing.current = false;
    };

    const renderSpace = (row: number, col: number, space: BoardSpace) => {
                const spaceWidth = props.width / props.boardState.boardData[row].length
                const spaceHeight = props.height / props.boardState.boardData.length
                return <Rect fill={space} x={col * spaceWidth} y={row * spaceHeight} width={spaceWidth} height={spaceHeight} />
    }

    const renderSpaces = () => {
        let spaces = [];
        for (let rowIndex = 0; rowIndex < props.boardState.boardData.length; rowIndex++) {
            const row = props.boardState.boardData[rowIndex];
            for (let colIndex = 0; colIndex < row.length; colIndex++) {
                const space = row[colIndex];
                if(space !== 'empty')
                {
                    spaces.push(renderSpace(rowIndex, colIndex, space))
                }            
            }
        }
        return spaces;
    }

    return (
        <div className="board-container text-center text-dark">

            <Stage
                width={props.width}
                height={props.height}
                onMouseDown={handleMouseDown}
                onMousemove={handleMouseMove}
                onMouseup={handleMouseUp}
                className="canvas-stage"
            >
                <Layer className='canvas-main'>
                    {renderSpaces()}
                </Layer>
            </Stage>
        </div>
    )
}

export default DrawingArea