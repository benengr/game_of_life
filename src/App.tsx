import React, { useEffect, useState, } from 'react';
import './App.css';
import Menu from './components/menu';
import Game, { BoardSpace } from './components/Game';

function App() {
  const game_id = "game_id_9183480"
  const SCALE = 0.95;
  const [boardHeight, setBoardHeight] = useState(0);
  const [boardWidth, setBoardWidth] = useState(0);

  let spaces: BoardSpace[][] = [];
  for (let row = 0; row < 20; row++) {
    let rowData: BoardSpace[] = [];
    for (let col = 0; col < 20; col++) {
      const n = row * col;
      if ((n % 3) === 0) {
        rowData.push('green')
      }
      else if ((n % 2) === 0) {
        rowData.push('blue')
      } else {
        rowData.push('empty')
      }
    }
    spaces.push(rowData);
  }

  function handleResize() {
    const container = document.getElementById(game_id)
    if (container !== null) {
      const h = container.clientHeight * SCALE;
      const w = container.clientWidth * SCALE;
      console.log(`Setting Container Height ${h}/${w}`)
      setBoardHeight(h);
      setBoardWidth(w);
    } else {
      console.warn("Could not get container id");
    }

  }
  useEffect(() => {
    handleResize();
    window.addEventListener("resize", handleResize)

  }, [])


  return (
    <div className="App">
      <header className="App-header">Game of Life</header>
      <div className="Body">
        <div className="Menu"><Menu /></div>
        <div className="Game" id={game_id}><Game height={boardHeight} width={boardWidth} boardState={{
          boardData: spaces,
          spaceShape: 'square'
        }} /></div>
      </div>
    </div>
  );
}

export default App;
